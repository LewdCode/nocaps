package src.me;


import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

	public void onEnable(){
		this.getServer().getPluginManager().registerEvents(this, this);
	}
	public void onDisable(){}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		StringBuilder sb = new StringBuilder();
		String message = e.getMessage();
		String[] msg = message.split(" ");
		if(e.getPlayer().hasPermission("nocaps.ignore")){}else{
			for(String s : msg){
				if(getServer().getPlayer(s) == null){
					s = s.toLowerCase();
					sb.append(s + " ");
				}else{
					sb.append(s + " ");
				}
				
				e.setMessage(sb.toString());
			}
		}
	}
}
